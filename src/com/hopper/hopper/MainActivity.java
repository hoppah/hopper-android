package com.hopper.hopper;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;


import com.hopper.hopper.model.Party;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import android.app.ActionBar;

public class MainActivity extends Activity {
	Context con;
	ArrayList<Party> partyList = null;
	ArrayList<Party> partyListBackup = null;
	//Will hold an un-touched copy of the sorted MatterObjects returned from the ASyncTask for the Filter to copy from
	ListView listView;
	//	SearchView searchView;
	PartyAdapter refreshedAdapter;
	PartyAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		con = this;
		setContentView(R.layout.activity_main);
		
		partyList = new ArrayList<Party>();

		ParseUser.enableAutomaticUser();
		ParseObject.registerSubclass(Party.class);
		Parse.initialize(this, "agcyOKxxvTBPRNeh6kyFH0UHp5leWX1VuIBrGiOI", "083PBZhLHiJOVknAGmO8G5rcxsjo36I4kQJz2TX9");
        
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_LOW);
        criteria.setSpeedRequired(false);
        criteria.setVerticalAccuracy(Criteria.NO_REQUIREMENT);
        
        ParseGeoPoint point = new ParseGeoPoint(42.3646, -71.1028);
        ParseQuery<Party> query = ParseQuery.getQuery("Party");
        query.whereWithinMiles(Party.locName, point, 10);
        try {
			partyList = (ArrayList) query.find();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		setMatterListViewAndContent(partyList);    
	}
	private void setMatterListViewAndContent(ArrayList<Party> newPartyList)
	{
		Log.d("d", "Setting listview and content");
		partyList = newPartyList;
		listView = (ListView) findViewById(R.id.LV_party_list);
		listView.setFastScrollEnabled(true);
		listView.setTextFilterEnabled(true);
		adapter = new PartyAdapter(this, R.layout.listformat_main, partyList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
//			public void onItemClick(AdapterView<?> arg0, View v, int position, long id)
//			{
//				Log.d("Dash.deb", "Item clicked");
//				try {
//					((MyOnRefreshListener) con).setRefreshComplete();
//					//					ContactServer.getMatter(matterList.get(position).getID(), con);
//				}
//				catch (Exception e) {}
//			}

			@Override
			public void onItemClick(AdapterView<?> arg0,
					android.view.View arg1, int arg2, long arg3) {
				Log.d("Dash.deb", "Item clicked");
				try {
					((MyOnRefreshListener) con).setRefreshComplete();
				}
				catch (Exception e) {}
				Intent i = new Intent(con, PartyDetails.class);
				Log.d("test", Double.toString(partyList.get(arg2).getLocation().getLatitude()));
				i.putExtra("Party", partyList.get(arg2));
				startActivity(i);
			}
		});

		//Set up searchview:
		//		searchView = (SearchView) findViewById(R.id.calendar_matters_SV);
		//		searchView.setOnQueryTextListener(new OnQueryTextListener() {
		//
		//			public boolean onQueryTextSubmit(String query) {
		//				return false;
		//			}
		//
		//			public boolean onQueryTextChange(String newText)
		//			{
		//				Log.d("Dash.deb", "Text changed to \""+newText+"\"");
		//				adapter.getFilter().filter(newText);
		//				return true;
		//			}
		//		});

//				//Setup Actionbar:
//				ActionBar ab = getSupportActionBar();
//				ab.setDisplayShowHomeEnabled(false);
//				ab.setDisplayShowTitleEnabled(false);
//				ab.setDisplayShowCustomEnabled(true);
//		
//				//Custom centered title on actionbar:
//				LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//				View v = inflater.inflate(R.layout.main_action_bar, null);
//		
////				((TextView) v.findViewById(R.id.actionbar_calendar_title)).setText("Matters");
//				ab.setCustomView(v);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		return false;
//		 Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	//Adapter:
	private class PartyAdapter extends ArrayAdapter<Party> implements SectionIndexer
	{
		private ArrayList<Party> partyArrayList;
		HashMap<String, Integer> alphaIndexer;
		String[] sections;

		public PartyAdapter(Context context, int layout, ArrayList<Party> obj)
		{
			super(context, layout);
			partyArrayList = obj;
			alphaIndexer = new HashMap<String, Integer>();
			int size = partyArrayList.size();

			if(size > 0)
			{
				if(partyArrayList.get(0) instanceof Party)
				{
					Collections.sort(partyArrayList);
					for (int i = size - 1; i >= 0; i--)
					{
						String element = partyArrayList.get(i).getTitle() == null ? "T" : partyArrayList.get(i).getTitle();
						//				Log.d("Dash.deb", "String to add (# " + i + ": " + element);
						//				Log.d("Dash.deb", "Substring to add (# " + i + ": " + element.substring(0, 1));
						alphaIndexer.put(element, i); //why substr 0, 1??

					}
					//			Log.d("Dash.deb", "HashMap Created: " + alphaIndexer.toString());

					Set<String> keys = alphaIndexer.keySet();
					//			Log.d("Dash.deb", "Key Set Created: " + keys.toString());

					Iterator<String> it = keys.iterator();
					ArrayList<String> keyList = new ArrayList<String>();

					while (it.hasNext())
					{
						String key = it.next();
						if (Character.isUpperCase(key.charAt(0)))
							keyList.add(key);
					}
					//			Log.d("Dash.deb", "Key List Created: " + keyList.toString());
					Collections.sort(keyList);
					sections = new String[keyList.size()];
					keyList.toArray(sections);
					//			Log.d("Dash.deb", "Key Array Created: " + sections.toString());
				}
			}

		}

		public int getCount() {
			int size = partyArrayList.size();
			return size;
		}

		public Party getItem(int position) {
			return partyArrayList.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		@SuppressWarnings("unchecked")
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				v = (View) vi.inflate(R.layout.listformat_main, null);
			}
			

			if(partyArrayList.get(position) instanceof Party)
			{
				Party p = partyArrayList.get(position);

				if (p != null) {
					//					TextView separator = (TextView) v.findViewById(R.id.lf_edit_matters_LS);
					TextView text = (TextView) (v.findViewById(R.id.lf_main_title));
					//					if (separator != null)
					//						separator.setText(o.getClient());
					if (text != null)
						text.setText(p.getTitle());
					
					TextView location = (TextView) (v.findViewById(R.id.lf_main_location));
					if(location != null)
						location.setText(p.getStreetAddress(con));
					
					TextView time = (TextView) (v.findViewById(R.id.lf_time));
					if(time != null) {
						SimpleDateFormat df1 = new SimpleDateFormat("M/dd hh:mm", Locale.getDefault());
						SimpleDateFormat df2 = new SimpleDateFormat("hh:mm", Locale.getDefault());
						Log.d("date", p.getStartTime().toString());
						Log.d("date", p.getEndTime().toString());
						time.setText(df1.format(p.getStartTime()) + " - " + df2.format(p.getEndTime()));
					}
					
					TextView percentage = (TextView) (v.findViewById(R.id.lf_main_percentage));
					if(percentage != null)
						percentage.setText( ( (int) (Math.round(100*( (double) (p.getUpvotes()) / (double) (p.getDownvotes()) )) ))+"%");
					//					if(position >= 1 && separator.getText().equals(matterArrayList.get(position-1).getClient()))
					//						separator.setVisibility(View.GONE);
					//					else 
					//						separator.setVisibility(View.VISIBLE);
				}
			}
			return v;
		}

		public int getPositionForSection(int position) {
			String letter = sections[position];
			Log.d("Dash.deb", "getPositionForSection " + position);
			return alphaIndexer.get(letter);
		}

		public int getSectionForPosition(int arg0) {
			Log.d("Dash.deb", "getSectionForPosition was called? WHY?");
			return 0;
		}

		public Object[] getSections() {
			return sections;
		}
	}
}
