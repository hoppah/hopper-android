package com.hopper.hopper;

import com.hopper.hopper.model.Party;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class PartyDetails extends Activity {
	Party p;
	TextView partyNameTV;
	TextView partyDescriptionTV;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_party_details);
		
		p = (Party) this.getIntent().getSerializableExtra("Party");
		
		if(p != null && p.getTitle() != null) {
			partyNameTV = (TextView) findViewById(R.id.TV_party_details_title);
			partyNameTV.setText(p.getTitle());
		}
		if(p!= null && p.getDescription() != null) {
			partyDescriptionTV = (TextView) findViewById(R.id.TV_party_details_description);
			partyDescriptionTV.setText(p.getDescription());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.party_details, menu);
		return true;
	}

}
