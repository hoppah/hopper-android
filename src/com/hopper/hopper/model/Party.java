package com.hopper.hopper.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.parse.ParseObject;
import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;

@ParseClassName("Party")
public class Party extends ParseObject implements Comparable, Serializable {
	private static final long serialVersionUID = -7825635054519064598L;
	public static String locName = "location";

	public Party() {

	}
	public Party(String name) {
		this.setTitle(name);
		this.setStartTime(new Date());
		this.setEndTime(new Date());
	}

	public void setTitle(String title) {
		put("title", title);
	}
	public String getTitle() {
		return getString("title");
	}

	public void setDescription(String description) {
		put("description", description);
	}
	public String getDescription() {
		return getString("description");
	}

	public void setHost(String host) {
		put("host", host);
	}
	public String getHost() {
		return getString("host");
	}

	public void setCost(int cost) {
		put("cost", cost);
	}
	public int getCost() {
		return getInt("cost");
	}

	public void addUpvote() {
		increment("upvotes");
	}
	public int getUpvotes() {
		return getInt("upvotes");
	}

	public void addDownvote() {
		increment("downvotes");
	}
	public int getDownvotes() {
		return getInt("downvotes");
	}

	public void setStartTime(Date time) {
		put("startTime", time);
	}
	public Date getStartTime() {
		return getDate("startTime");
	}

	public void setEndTime(Date time) {
		put("endTime", time);
	}
	public Date getEndTime() {
		return getDate("endTime");
	}


	public void setLocation(double lat, double lon) {
		put("location", new ParseGeoPoint(lat, lon));
	}
	public ParseGeoPoint getLocation() {
		return getParseGeoPoint("location");
	}

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getStreetAddress(Context con) {
		Log.d("test", con.toString());
		if (!Geocoder.isPresent()) {
			return "No Address Available";
		}

		ParseGeoPoint point = getLocation();
		if(point != null)
			Log.d("location", Double.toString(point.getLatitude()));
		else Log.d("location", "Point is null");

		Geocoder geocoder = new Geocoder(con, Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(point.getLatitude(), point.getLongitude(), 1);
			Address address = addresses.get(0);
			String addrString = "";
			int longestAddressLine = address.getMaxAddressLineIndex();
			for (int i = 0; i < longestAddressLine; i++) {
				addrString = addrString + address.getAddressLine(i) + " ";
			}
			return addrString;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return "No Address Available";
		}
	}

}
