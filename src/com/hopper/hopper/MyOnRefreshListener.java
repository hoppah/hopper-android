package com.hopper.hopper;

import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshAttacher.OnRefreshListener;

public interface MyOnRefreshListener extends OnRefreshListener {
	void setRefreshComplete(); //this method should simply call the mPullToRefreshAttacher's setRefreshComplete method.
}
